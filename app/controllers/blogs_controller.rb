class BlogsController < ApplicationController

	def index
		blogs = URI("http://mapprr.ws/blogs")
		blogs = Net::HTTP.get(blogs)
		@blogs = JSON.parse(blogs)['blogs'].reverse
	end

	def create
	end

	def show
		blog_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/blogs/#{params['title']}" ), {})
		@blog_data = JSON.parse(blog_data.body)
		if @blog_data['blog'].present?
			@blog = @blog_data['blog']
		else
			@blog = nil
		end
	end

	def post_to_blog
	    cover_image = 'blog.jpg'
	    if blog_params['cover_image'].present?
			obj = S3_BUCKET.objects[blog_params['cover_image'].original_filename]
	    
		    obj.write(
		      file: blog_params['cover_image'],
		      acl: :public_read
		    )
		    cover_image = "https://s3.ap-south-1.amazonaws.com/mapprr/" + blog_params['cover_image'].original_filename
	    end
	    @blog={
	    	"title" => blog_params['title'].upcase,
	    	"content" => blog_params['content'],
	    	"cover_image" => cover_image
	    }
	    blog = Net::HTTP.post_form(URI.parse("http://mapprr.ws/blogs?blog="), @blog)
		redirect_to blogs_path
	end

	def edit
		blog_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/blogs/#{params['title'].parameterize}" ), {})
		blog_data = JSON.parse(blog_data.body)
		if blog_data['blog'].present?
			@blog = blog_data['blog']
		else
			@blog = nil
		end
	end

	def update
		@blog={
	    	"title" => blog_params['title'].upcase,
	    	"content" => blog_params['content'],
	    	"description" => blog_params['description'],
	    	"keywords" => blog_params['keywords']
	    }
	    blog_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/blogs/change?id="+params[:id] ), @blog)
	    redirect_to blogs_path
	end

	def destroy
	end

	private
	def blog_params
		params.require(:blog).permit(:title, :content, :cover_image, :description, :keywords)
	end
end