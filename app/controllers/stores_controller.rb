class StoresController < ApplicationController
	#before_action :authenticate_user
  	def index
  		if params[:query].present?
		  	key_params = encode_key()
			@stores_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/stores/get_store_details?key="+key_params), {})
			# binding.pry
			@stores_data = JSON.parse(@stores_data.body)
			stores = @stores_data['stores']
			if stores.present?
				distance_params = params["query"].split(",")[2,2].join(',')
				store_distances = ""
				stores.each do |store|
					store_distances += store["location"]["lat"].to_s + "," + store["location"]["lng"].to_s + "|"
				end
				distance_data = Net::HTTP.post_form(URI.parse("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+distance_params+"&destinations="+store_distances+"&key=AIzaSyAZUpobR20JfD78lRFdkVU2VV1joDsBVjA"), {})
				distance_data = JSON.parse(distance_data.body)
				@distances = []
				distance_data["rows"].first["elements"].each do |dd|
					@distance = []
					@distance << dd["distance"]["text"]
					@distance << dd["duration"]["text"]
					@distances << @distance
				end
				for i in 0..stores.length-1
					stores[i].merge!("distance" => @distances[i], "address" => distance_data["destination_addresses"][i])
				end
			end
			stores = stores.sort_by{|store| store['distance']}
			@stores =[]
			stores.each do |store|
				@stores << store if store['is_bookmarked']
			end
			stores.each do |store|
				@stores << store unless store['is_bookmarked']
			end
			@similar_products = @stores_data['similar_products']
			# binding.pry
		else
			@stores = "404"
		end
		# puts stores
	end

	def my_profile
		key_params = encode_key()
		redirect_to profile_path(query: key_params)
	end

	def profile
		key_params = params["query"]
		response_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/consumers/get_profile_info?key="+key_params), {})
		response_data = JSON.parse(response_data.body)
		#binding.pry
		@name = response_data["name"]
		@dob = response_data["dob"]
		@mobile = response_data["mobile"]
		@gender = response_data["gender"]
		@recent_products = response_data["recent_products"]
		#binding.pry
	end

	def set_profile
		key_params = encode_key()
		response_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/consumers/set_profile?key="+key_params), {})
		# binding.pry
		render status: response_data.code.to_i, json:{"message" => JSON.parse(response_data.body)['message'], "activation_status" => JSON.parse(response_data.body)['activation_status']}
	end

	def set_name_profile
		key_params = encode_key()
		response_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/consumers/set_name?key="+key_params), {})
		render status: response_data.code.to_i, json:{"message" => JSON.parse(response_data.body)['message']}
	end

	def set_mobile_profile
		key_params = encode_key()
		response_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/consumers/set_mobile?key="+key_params), {})
		render status: response_data.code.to_i, json:{"message" => JSON.parse(response_data.body)['message']}
	end

	def set_favourite
		key_params = encode_key()
		response_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/stores/set_favourite?key="+key_params), {})
		render status: response_data.code.to_i, json:{"message" => JSON.parse(response_data.body)['message']}
	end

	def remove_favourite
		key_params = encode_key()
		response_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/stores/remove_favourite?key="+key_params), {})
		render status: response_data.code.to_i, json:{"message" => JSON.parse(response_data.body)['message']}
	end

	def favourites_list
		if params[:query].present?
			key_params = encode_key()
			response_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/stores/favourites_list?key="+key_params), {})
			response_data = JSON.parse(response_data.body)
			favourite_stores = response_data['favourite_stores']
			if favourite_stores.present?
				distance_params = params["latlng"]
				store_distances = ""
				favourite_stores.each do |store|
					store_distances += store["location"]["lat"].to_s + "," + store["location"]["lng"].to_s + "|"
				end
				distance_data = Net::HTTP.post_form(URI.parse("https://maps.googleapis.com/maps/api/distancematrix/json?origins="+distance_params+"&destinations="+store_distances+"&key=AIzaSyAZUpobR20JfD78lRFdkVU2VV1joDsBVjA"), {})
				distance_data = JSON.parse(distance_data.body)
				@distances = []
				distance_data["rows"].first["elements"].each do |dd|
					@distance = []
					@distance << dd["distance"]["text"]
					@distance << dd["duration"]["text"]
					@distances << @distance
				end
				for i in 0..favourite_stores.length-1
					favourite_stores[i].merge!("distance" => @distances[i], "address" => distance_data["destination_addresses"][i])
				end
			end
			@favourite_stores = favourite_stores.sort_by{|store| store['distance']}
		else
			@favourite_stores = "404"
		end
	end

	def feedback
		key_params = encode_key()
		response_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/consumers/feedback?key="+key_params), {})
		response_data = JSON.parse(response_data.body)
	end

	def show
	    store_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/stores/get_store?store_area="+params[:store_area]+"&name="+params[:name]), {})
	    @store_data = JSON.parse(store_data.body)
	    if @store_data["store"].present?
	    	# binding.pry
	      @store = @store_data["store"]
	    else
	      @store = nil
	    end
	end

	private

	  	def encode_key
	  		query = params["query"]
	  		iv = '1234567890123456'
	  		api_key_hash = {"mapprr"=>"12345678908905672341765890432165"}
	  		key = api_key_hash["mapprr"]
	  		acipher = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
		      acipher.encrypt
		      acipher.key = key
		      acipher.iv = iv
	     	encrypted_string = acipher.update(query) + acipher.final
	     	val = Base64.encode64(encrypted_string).strip
	     	val
	  	end

	  	def store_params
  			params.permit(:query)
  		end
end
