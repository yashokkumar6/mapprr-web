class UsersController < ApplicationController
	def contact_us
		UserMailer.send_contact_query(params).deliver_now
	end
end