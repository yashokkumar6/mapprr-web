class HomeController < ApplicationController
  def index
    blogs = URI("http://mapprr.ws/blogs")
    blogs = Net::HTTP.get(blogs)
    @blogs = JSON.parse(blogs)['blogs'].last(5).reverse.last(3)
  end

  def user_login
  	key_params = encode_key()
  	login_data = Net::HTTP.post_form(URI.parse("http://mapprr.ws/users?key="+key_params), {})
    login_data = JSON.parse(login_data.body)
    if login_data["otp"].present? || login_data["user_id"].present?
    	render status: 200, json:{"message" => login_data}
    else
    	render status: 422, json: {"message" => "Parameters Mismatch"}
    end
  end

  def terms  
  end

  def privacy
  end

  def login
    if request.referer.present?
      if URI(request.referer).path.include? "/stores"
        @pass = "false"
        @query = URI(request.referer).query
      else
        @pass = "true"
      end
    else
      @pass = "true"
    end
  end

  def team
  end

  def careers
  end

  def send_resume
    UserMailer.careers_mail(params).deliver_now
    redirect_to careers_path
  end

  private
  	def encode_key
  		query = search_params["mobile"] + ",consumer"
  		if search_params["otp"].present?
  			query += "," + search_params["otp"]
  		end
  		iv = '1234567890123456'
  		api_key_hash = {"mapprr"=>"12345678908905672341765890432165"}
  		key = api_key_hash["mapprr"]
  		acipher = OpenSSL::Cipher::Cipher.new("aes-256-cbc")
	      acipher.encrypt
	      acipher.key = key
	      acipher.iv = iv
     	encrypted_string = acipher.update(query) + acipher.final
     	val = Base64.encode64(encrypted_string).strip
     	val
  	end

  	def search_params
  		params.permit(:mobile, :otp)
  	end
end
