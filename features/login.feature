@headmode
Feature: Login / Sign Up

  # Scenario: Should not login to an account with invalid number
  #   Given I visit the homepage
  #   Then I set the location
  #   Then I click on "Register"
  #   Then I fill up the "mobile" field with "6231231234"
  #   Then I click on "Get OTP"
  #   Then I should not see "Login Button" on screen
  Scenario: Should login to an account with valid number
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "9231231234"
    Then I click on "Get OTP"
    Then I should see "Login Button" on screen

  Scenario: User should be able to login with an invalid OTP
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "9342342222"
    Then I click on "Get OTP"
    Then I enter the wrong OTP
    Then I click on "Login"
    Then I should see "Invalid OTP" on screen

  Scenario: User should be successfully logged in
    Given I visit the homepage
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I should see "User Logged in" on screen