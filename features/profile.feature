@headmode
Feature: Profile Section Test Scenarios

  Scenario: Changing the date of birth on the profile page
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I click on "User Name"
    Then I click on "Profile Info"
    Then I change the DOB to "22/01/2015"
    Then I click on "Save Changes"
    Then I refresh the page
    Then I should see the dob changed as "22/01/2015"
    Then I logout

  Scenario: Changing name in Personal Info Section
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I click on "User Name"
    Then I click on "Profile Info"
    Then I click on "Edit Name Icon"
    Then I edit the "Full Name" as "Test Subject"
    Then I click on "Save Changes"
    Then I click on "Back to Search"
    Then I should see "Test Subject" on screen
    Then I logout

  Scenario: Name should be minimum three characters long and non empty
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I click on "User Name"
    Then I click on "Profile Info"
    Then I click on "Edit Name Icon"
    Then I edit the "Full Name" as ""
    Then I click on "Save Changes"
    Then I should see "Minimum at least 3 characters" on screen
    Then I logout

  Scenario: Changing the gender on the profile page
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I click on "User Name"
    Then I click on "Profile Info"
    Then I change the Gender to "Female"
    Then I click on "Save Changes"
    Then I logout