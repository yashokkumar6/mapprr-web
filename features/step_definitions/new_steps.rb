
Given("I visit the homepage") do
  visit 'https://qa.mapprr.com/'
  sleep(3)
  sleep 1
  maximize_and_center_screen unless maximised
end

def maximize_and_center_screen
  screen_width = page.driver.browser.execute_script("return screen.width;")
  screen_height = page.driver.browser.execute_script("return screen.height;")
  page.driver.browser.manage.window.resize_to(screen_width,screen_height)
  page.driver.browser.manage.window.move_to(0,0)
end

def maximised
  screen_width = page.driver.browser.execute_script("return screen.width;")
  if page.driver.browser.manage.window.size.width == screen_width && page.driver.browser.manage.window.position.x == 0
    true
  else
    false
  end
end

def image
  Capybara.save_and_open_screenshot
end

Given(/^I click on "([^"]*)"$/) do |arg|
  case arg
    when 'Favourites'
      page.find('#fav').click
    when 'Register'
      page.find('#user_login').click
    when 'Get OTP'
      click_button 'Get OTP'
    when 'Login'
      click_button 'Login'
    when 'User Name'
      page.find('.user_name').click
    when 'Profile Info'
      page.find('#profile_link').click
    when 'Edit Name Icon'
      page.find('#name_edit').click
    when 'Save Changes'
      page.find('#submit_info').click
    when 'Back to Search'
      page.find('.empty_state').click
  end
end

Then("I add the store in favorites") do
    card = page.all('.card-block').first
    @store_name = card.find('.store-title').text
    if card.has_css?('.fa-heart-o')
      print('Adding '+@store_name+' to favourites')
      card.find('.fa-heart-o').click
    else
      print(@store_name+' already in favorites')
    end
end

Then("I remove the stores in favorites") do
    until page.all('.card-block').size == 0
      card = page.all('.card-block').first
      card.find('.fa-heart').click
      sleep 2
    end
end

Then("I should find the card on the favourites") do
  card = page.all('.card-block').first
  store_name = card.find('.store-title').text
  print('Found '+store_name)
  assert store_name == @store_name
end

Then("I edit the {string} as {string}") do |string, value|
  case string
    when 'Full Name'
      name = page.find('#full_name')
      name.set('')
      name.set(value)
  end
end

Then("I refresh the page") do
  visit current_url
end

Then("I logout") do
  page.find('.user_name').click
  page.find('#logout').click
  sleep 1
end

Then("I should see the dob changed as {string}") do |value|
  assert page.find("#dob").value == value
end

Then("I change the DOB to {string}") do |date|
  page.find('#name_edit').click
  page.find('#dob').set(date)
  page.find('#full_name').click
end

Then("I enter the OTP") do
  page.find('#otp').set(123456)
end

Then("I enter the wrong OTP") do
  page.find('#otp').set(111111)
end

Then("I set the location") do
  page.find('#geolocation').click
  page.find('#m_loc').send_keys('madhapur')
  page.find('#m_loc').send_keys(:down)
  page.find('#m_loc').send_keys(:enter)
end


def wait_until_condition(seconds, condition)
  case condition
    when 'loader_complete'
      while page.all('.loader').size == 0 || seconds == 0
        sleep 1
        seconds = seconds - 1
      end
    when 'OTP validator'
      while page.all('#help-block').size == 0 || seconds == 0
        sleep 1
        seconds = seconds - 1
      end
  end
end


Then(/^I fill up the "([^"]*)" field with "([^"]*)"$/) do |field, value|
  case field
    when 'mobile'
      page.find('#mobile').set(value)
  end
end

Then(/^Debugger$/) do
  binding.pry
end

Then(/^I should not see "([^"]*)" on screen$/) do |arg|
  assert !page.has_button?('Login')
end

Then(/^I should see "([^"]*)" on screen$/) do |arg|
  case arg
    when 'Login'
      assert page.has_button?('Login')
    when 'Invalid OTP'
      wait_until_condition(5,'OTP validator')
      assert page.find('#help-block').text == arg
    when 'User Logged in'
      assert page.has_css?'#user_login'
    when 'Test Subject'
      assert page.find('.user_name').text.include? arg
    when 'Minimum at least 3 characters'
      assert page.find('#error').text.include? arg
    when 'Stores'
      assert page.has_css?'.store-card'
    when 'Product not available at your nearest store'
      assert page.find('.empty_state').text.include? arg
    when 'Your Favourite List Is Empty.'
      assert page.all('.empty_state').last.text.include? arg
  end
end

Then("I should see the product {string}") do |string|
  assert page.all('.pcard-block')[0].text.include?string.upcase
end

Then("I change the Gender to {string}") do |gender|
  page.find('#name_edit').click
  case gender
    when 'Male'
      page.find(".male").click
    when 'Female'
      page.find(".female").click
  end
end

Then("I click on the product {string}") do |string|
  page.all('.pcard-block')[0].click
end

Then("I search for product named {string}") do |product|
  search_box = page.find('#search_query')
  arr = product.chars.to_a
  arr.each do |a|
      search_box.send_keys(a)
  end
  search_box.send_keys(:down)
  search_box.send_keys(:down)
  search_box.send_keys(:enter)
  page.find('.searchbtn').click
end
