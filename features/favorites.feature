@headmode
Feature: This is where the favourites section will be tested

  Scenario: Favourites Section should have stores if there are favourites
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I search for product named "Crocin"
    Then I click on the product "Crocin"
    Then I add the store in favorites
    Then I click on "User Name"
    Then I click on "Favourites"
    Then I should find the card on the favourites
    Then I logout

  Scenario: Removing a store from favourites
    Given I visit the homepage
    Then I set the location
    Then I click on "Register"
    Then I fill up the "mobile" field with "1234567890"
    Then I click on "Get OTP"
    Then I enter the OTP
    Then I click on "Login"
    Then I click on "User Name"
    Then I click on "Favourites"
    Then I remove the stores in favorites
    Then I should see "Your Favourite List Is Empty." on screen
    Then I logout
