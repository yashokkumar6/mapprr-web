require "rails/all"
require "capybara/cucumber"
require "selenium/webdriver"
require "capybara/poltergeist"
require 'pry'
require 'minitest/autorun'
require 'capybara-screenshot/cucumber'

Capybara.default_max_wait_time = 10
Capybara.default_driver = :headless_chrome

Capybara.register_driver :firefox do |app|
  Selenium::WebDriver::Firefox.driver_path = 'features/support/geckodriver'
  client = Selenium::WebDriver::Remote::Http::Default.new
  client.open_timeout = 90 # instead of the default 60
  Capybara::Selenium::Driver.new(app, browser: :firefox, http_client: client)
  # Capybara::Selenium::Driver.new(app, browser: :firefox)
end

driver_name = :headless_chrome
browser_name = :chrome
screen_size = [1920, 1080]


driver_capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
    chromeOptions: {
      args: %w(headless disable-gpu no-sandbox),
      binary:  ENV.fetch('GOOGLE_CHROME_SHIM', nil)
    }.reject { |_, v| v.nil? }
  )

Capybara.register_driver driver_name do |app|
  Capybara::Selenium::Driver.new(
    app,
    browser: browser_name,
    desired_capabilities: driver_capabilities
  ).tap do |driver|
    driver.browser.manage.window.size = Selenium::WebDriver::Dimension.new(*screen_size)
  end
end

Capybara.register_driver :chrome do |app|
  Capybara::Selenium::Driver.new(
    app,
    browser: browser_name
  ).tap do |driver|
    driver.browser.manage.window.size = Selenium::WebDriver::Dimension.new(*screen_size)
  end
end

Capybara.javascript_driver = :headless_chrome

Capybara::Screenshot.register_driver(:headless_chrome) do |driver, path|
  driver.browser.save_screenshot(path)
end
Capybara::Screenshot.register_driver(:chrome) do |driver, path|
  driver.browser.save_screenshot(path)
end
Capybara.save_path = 'Screenshots'


Before ('@headless') do
  Capybara.current_driver = :headless_chrome
end


Before ('@headmode') do
  Capybara.current_driver = :chrome
end
